package com.login_servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.HttpSession;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class order_product
 */
@WebServlet("/order_product")
public class order_product extends HttpServlet {
	private static final long serialVersionUID = 1L;
	String base_url = "http://localhost:8080/ordering_system/";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public order_product() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String	add		=	request.getParameter("create");
		String	update	=	request.getParameter("update");
		String	delete	=	request.getParameter("delete");
		
		String	deleteOrder	=	request.getParameter("deleteOrder");
		String	returnMain	=	request.getParameter("return");
		
		String	productID	=	request.getParameter("productID");
		String	quantity	=	request.getParameter("quantity"); 
		
		System.out.println(productID);
		System.out.println(quantity);
		
		int query_result = 0;
		
		
		Database db = null;
		
		if(add!=null)
		{
			try 
			{
				db = new Database();
				query_result	=	db.addProduct(request, productID, quantity);
			} 
			catch (ClassNotFoundException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} 
			catch (SQLException e1) 
			{
			// TODO Auto-generated catch block
			e1.printStackTrace();
			}
		
			if(query_result == 1)
			{
				response.sendRedirect(base_url + "new_order.jsp");
			}
			else if(query_result == 0)
			{
				response.sendRedirect(base_url + "quantity_unavailable.jsp");
			}
		}
		else if(delete!=null)
		{
			try {
				db = new Database();
				db.update_products_sold_delete(productID);
				db.delete_product(request, productID);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			response.sendRedirect(base_url + "new_order.jsp");
			
		}
		else if(deleteOrder!=null)
		{
			try {
				db	=	new	Database();
				db.delete_order(request);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			response.sendRedirect(base_url + "salesperson.jsp");
			
			
		}
		else if(returnMain!=null)
		{
			response.sendRedirect(base_url+"salesperson.jsp");
		}
		
		
		
	}

}
