package com.login_servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class file_complaint
 */
@WebServlet("/file_complaint")
public class file_complaint extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public file_complaint() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String	complain	=	request.getParameter("complaint");
		
		if(complain!=null)
		{
			String	complaintDate	=	request.getParameter("complaintDate");
			String	againstEmail	=	request.getParameter("complaintAgainst");
			String	complaintDescr	=	request.getParameter("complaintDescr");
			
			try 
			{
				Database	db = new Database();
				db.create_complaint(request, againstEmail, complaintDate, complaintDescr);
			}
			catch (ClassNotFoundException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (SQLException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String	url	=	"http://localhost:8080/ordering_system/salesperson.jsp";
			response.sendRedirect(url);
					
		}
	}

}
