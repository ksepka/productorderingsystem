package com.login_servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Login_servlet
 */
@WebServlet("/Login_servlet")
public class Login_servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login_servlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Database db = null;
		String eml = request.getParameter("email");
		String pw = request.getParameter("password");
		String	firstName	=	request.getParameter("firstName");
		String	lastName	=	request.getParameter("lastName");
		String	user_type	=	null;
		String msg = " ";
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession(true);

		session.setAttribute("sessionEmail", eml);
		try{
			db = new Database();
			
			String name = db.getNameByEmail(eml);
			session.setAttribute("name", name);
			System.out.println(name);
			
			if(db.grantAccess(eml, pw)){
				try {
					user_type = db.get_user_type(eml);
					if(user_type.equals("Client") )
					{
						response.sendRedirect("client.jsp");
					}
					else if(user_type.equals("Salesperson"))
					{	
						response.sendRedirect("salesperson.jsp");
					}
					else if(user_type.equals("Manager"))
					{ 
						response.sendRedirect("manager.jsp");
					}
					else if(user_type.equals("Director"))
					{
						response.sendRedirect("director.jsp");
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			else{
				msg = "Login failed.  Please try again or register for a new account";
				response.setContentType("text/html");
				out.println("<font size='6' color=red>" + msg);
			}
			db.disconnect();
		}
		catch (Exception e){
			out.println("Exception: " + e);
		}
	} //End of service


//	private String getAbsoluteUrl(String relativeUrl){
//		return baseUrl + relativeUrl;
//
//
//	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession result = request.getSession(true);
		String msg = " ";
		PrintWriter out = response.getWriter();
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		Database db = null;
		

		if (email.isEmpty() || password.isEmpty()){
			msg = "There are empty fields.  Please try again";
			response.setContentType("text/html");
			out.println("<font size='6' color=red>" + msg);
		}
		else{
			try{
				db = new Database();
				if(db.isEmailRegistered(email)){
					String username = db.getNameByEmail(email);
					result.setAttribute("sessionName", username);
					result.setAttribute("sessionEmail", email);
				}
				else{
					msg = "This email address is not registered.  Please try again or register for a new account";
					response.setContentType("text/html");
					out.println("<font size='6' color=red>" + msg);
				}

				db.disconnect();
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
		} //End of else
		
	}

}
