package com.login_servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class insert_product
 */
@WebServlet("/insert_product")
public class insert_product extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public insert_product() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

				
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String	create	=	request.getParameter("create");
		String	update	=	request.getParameter("update");
		
		String	productID			=	request.getParameter("productID");
		String	productCateg		=	request.getParameter("productCateg");
		String	productName			=	request.getParameter("productName");
		String	productDescr		=	request.getParameter("productDescr");
		String	productQuantity		=	request.getParameter("productQuantity");
		String	productPrice		=	request.getParameter("productPrice");
		String	productAvailable	=	request.getParameter("productAvailable");
	
		
		if(create!=null)
		{	
			if(productID!=null && productCateg!=null && productName!=null &&
				productDescr!=null && productQuantity!=null && productPrice!=null 
				&& productAvailable!=null)
			{
				Database insert = null;
			
				
				try {
					insert = new Database();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				System.out.println("successful databse instance");
				
				int query_status = 0;
				
				try {
					query_status	=	insert.insert_product(productID, 
								productCateg, productName, 
							productDescr, productQuantity, productPrice, productAvailable);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
					System.out.println("inser function called");
			
				
				if(query_status==1)
				{
					String	url	=	"http://localhost:8080/ordering_system/product_added.jsp";
					response.sendRedirect(url);
				}
				else if(query_status!=1)
				{
					String	url	=	"http://localhost:8080/ordering_system/product_added_fail.jsp";
					response.sendRedirect(url);
				}
			}
		}
		else if(update!=null)
		{	
			System.out.println(productPrice);
			System.out.println(productQuantity);
			System.out.println(productAvailable);
			
			Database	update_prod	=	null;
			
			try {
				update_prod	=	new Database();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("fail database instance");
			}
			
			int	query_result	=	0;
			
			try {
				query_result	=	update_prod.director_update_product( productID,productPrice, productQuantity,  productAvailable);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("fail update funciton");
			}
			
			if(query_result!=1)
			{
				String	url	=	"http://localhost:8080/ordering_system/update_product_fail.jsp";
				response.sendRedirect(url);
			}
			else
			{
				String	url	=	"http://localhost:8080/ordering_system/director.jsp";
				response.sendRedirect(url);
			}
			
		}
	}

}
