package com.login_servlet;

import java.io.IOException;

import javax.servlet.http.HttpSession;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class new_order
 */
@WebServlet("/new_order")
public class new_order extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public new_order() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		HttpSession	session =  request.getSession(true);
		
		String	orderID		=	request.getParameter("orderID");
		String	clientEmail	=	request.getParameter("clientEmail");
		String	clientCo	=	request.getParameter("clientCo");
		String	date		=	request.getParameter("date");
		String	createOrder	=	request.getParameter("create");
		
		if(createOrder!=null && !orderID.isEmpty() && !clientEmail.isEmpty() && !clientCo.isEmpty() && !date.isEmpty() )
		{
			session.setAttribute("orderID", orderID);			// Save order id in session to make an order
			session.setAttribute("clientEmail", clientEmail);	// Save clientEmail in session to make order
			session.setAttribute("clientCo", clientCo);			// Save client Company in session to make order
			session.setAttribute("date", date);					// Save date in session to make an order
		
			String	url	=	"http://localhost:8080/ordering_system/new_order.jsp";
			response.sendRedirect(url);
		}
		
	}

}
