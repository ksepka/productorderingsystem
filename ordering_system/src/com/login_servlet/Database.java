package com.login_servlet;

import java.sql.*;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;

public class Database 
{
	Connection	con;
	PreparedStatement sql_query;
	ResultSet rs; 
	
	public Database() throws ClassNotFoundException, SQLException
	{
		connect();
	}
	
	public void connect() throws SQLException,ClassNotFoundException{
		String db_user;
        String db_pass;
                
        db_pass = "";
        db_user = "root";
		
		Class.forName("com.mysql.jdbc.Driver");
		
		con	=	DriverManager.getConnection
            ("jdbc:mysql://localhost:3306/product_ordering_system", 
                    db_user,db_pass);
	}
	
    public void disconnect() throws SQLException, ClassNotFoundException{
        con.close();
    }
    
	
	// ------------------------------------------------------------ LOGIN & REGISTRATION ----------
	
    public boolean grantAccess(String email, String passw) throws SQLException{
    	String sql = "SELECT * FROM User WHERE email='"+ email +"' AND password='" + passw +"'";
        PreparedStatement ps = con.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
     	
        if (rs.next()){
        	return true;
        }
       
    	return false;
    }
	
    //Checks whether a given email has already registered on the site
    public boolean isEmailRegistered(String email) throws SQLException{
        String sql = "SELECT * FROM User WHERE email='"+ email +"'";
        PreparedStatement ps = con.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
 
        return rs.next();
    }
    
    //Gets full name of user from given email, for the purpose of displaying on pages
    public String getNameByEmail(String email) throws SQLException{
    	String sql = "SELECT * FROM User WHERE email='" + email + "'";
    	PreparedStatement ps = con.prepareStatement(sql);
    	ResultSet rs = ps.executeQuery();
    	
    	if (rs.next()){
    		String first = rs.getString("firstName");
    		String last = rs.getString("lastName");
    		String name = first + " " + last;
    		return name;
    	}
    	
		return null;
    }
	
    //Register a new client
    public void registerUser(String firstn, String lastn,  String email, String userType, String password) throws SQLException{
        String sql = "INSERT INTO User (email, firstName, lastName, userType, password) "
        		+ "VALUES ('"+ email +"', '"+ firstn +"', '"+ lastn +"', '"+ userType +"', '"+ password +"')";
        PreparedStatement ps = con.prepareStatement(sql);
        ps.executeUpdate();
    }
	
	
	public String get_user_type(String email) throws SQLException
	{
		String sql_statement = "SELECT * FROM User WHERE email= '" + email + "'";
		sql_query = con.prepareStatement(sql_statement);
		rs = sql_query.executeQuery();
		
		String	user_type	=	null;
		
		while (rs.next()) 
		{
			user_type	=	rs.getString("userType"); 
	    }
				
		return user_type;
	}
	
	//-------------------------------------------------------------------- PRODUCTS ---------------
	
	
	// Insert a product into the inventory
	public int insert_product(String productID,String productCateg, String productName, String productDescr,
			String productQuantity, String productPrice, String productAvailable ) throws SQLException
	{
		String productsSold = "0";
		
		String sql_statement = "INSERT INTO Product VALUES (?,?,?,?,?,?,?,?);";
		sql_query	=	con.prepareStatement(sql_statement);
		
		sql_query.setString(1,productID);
		sql_query.setString(2,productCateg);
		sql_query.setString(3,productName);
		sql_query.setString(4,productDescr);
		sql_query.setString(5,productQuantity);
		sql_query.setString(6,productPrice);
		sql_query.setString(7,productAvailable);
		sql_query.setString(8,productsSold);
		
		int query_status = sql_query.executeUpdate();
		
		return query_status;
		
	}
	
	
	// Update a product already in the inventory
	public int update_product(String productID, String new_quantity,String new_price,
			String new_available) throws SQLException
	{
		int	query_status	=	0;
		
		if(new_quantity!=null)
		{
			String sql_statement = "UPDATE Product SET productQuantity = '" + new_quantity +"' WHERE productID = " + productID +";";
			sql_query = con.prepareStatement(sql_statement);
			query_status = sql_query.executeUpdate();
		}
		if(new_price!=null)
		{
			String sql_statement = "UPDATE Product SET productPrice = '" + new_price +"' WHERE productID = " + productID +";";
			sql_query = con.prepareStatement(sql_statement);
			query_status = sql_query.executeUpdate();
		}
		if(new_available!=null)
		{
			String sql_statement = "UPDATE Product SET productAvailable = '" + new_available +"' WHERE EXISTS productID = " + productID +";";
			sql_query = con.prepareStatement(sql_statement);
			query_status = sql_query.executeUpdate();
		}
		
		return query_status;
	}
	
//=============================================ORDERS===============================================================	
	
	// Add a product to an order
	public int addProduct(HttpServletRequest request, String productID, String quantity) throws SQLException
	{
		HttpSession session = request.getSession(true);
		
		int	query_result	=	0;
		
		String	sql_statement	=	"SELECT productName, productQuantity, productPrice from Product WHERE productID = '" + productID + "';";
		sql_query = con.prepareStatement(sql_statement);
		rs = sql_query.executeQuery();
		
		String	productName		=	null;
		String	productPrice	=	null;
		String	productQuantity	=	null;
		
		while (rs.next()) 
		{
			productName		=	rs.getString("productName").toString(); 
			productQuantity	=	rs.getString("productQuantity").toString();
			productPrice	=	rs.getString("productPrice").toString(); 
		}
		
		int		orderQuantity = Integer.valueOf(quantity);
		int		convtProductQuantity	=	Integer.valueOf(productQuantity);
		
		if(orderQuantity <= convtProductQuantity)
		{
			double	price	= Double.valueOf(productPrice);
			double	totalPrice	=	0;
			
			System.out.println("order quantity is less than available product");
			
			String	orderID		=	(String)session.getAttribute("orderID");
			String	clientEmail	=	(String)session.getAttribute("clientEmail");
			String	clientCo	=	(String)session.getAttribute("clientCo");
			String	orderDate	=	(String)session.getAttribute("date");
		
			String	salespersonEmail	=	(String)session.getAttribute("sessionEmail");
	
			totalPrice	=	orderQuantity * price;	
			
			String convtTotalPrice = String.format( "%.2f", totalPrice );
			
			String	convtOrderQuantity	=	Integer.toString(orderQuantity);
		
			sql_statement	=	"INSERT INTO orders VALUES(?,?,?,?,?,?,?,?,?,?)";
			sql_query		=	con.prepareStatement(sql_statement);
		
			sql_query.setString(1, orderID);
			sql_query.setString(2, orderDate);
			sql_query.setString(3, clientCo);
			sql_query.setString(4, clientEmail);
			sql_query.setString(5, salespersonEmail);
			sql_query.setString(6, productID);
			sql_query.setString(7, productName);
			sql_query.setString(8, convtOrderQuantity);
			sql_query.setString(9, productPrice);
			sql_query.setString(10, convtTotalPrice);
		
			query_result	=	sql_query.executeUpdate();

			update_products_sold(productID, convtOrderQuantity);
			update_product_subtract_quantity(productID, convtOrderQuantity);
		}
		else
		{
			System.out.println("The quantity for that product is unavailable. Choose another quantity");
			query_result	=	0;
			
		}	
		return query_result;
	}
	
	
	// Update a product in a current order
	public void update_product(String productID, String quantity) throws SQLException
	{
		String	sql_statement	=	"UPDATE orders SET orderQuantity = '" + quantity + "' WHERE productID ='" + productID + "';";
		sql_query = con.prepareStatement(sql_statement);
		sql_query.executeUpdate();
		
	}
	
	
	// Delete a product in a current order
	public void delete_product(HttpServletRequest request, String productID) throws SQLException
	{	
		HttpSession session = request.getSession(true);
		
		String	orderID			=	(String)session.getAttribute("orderID");
		String	quantity	=	null;
		
		String	sql_statement	=	"SELECT orderQuantity from orders WHERE productID = '" + productID + "';";
		sql_query	=	con.prepareStatement(sql_statement);
		rs	=	sql_query.executeQuery();
		
		while(rs.next())
		{
			quantity	=	rs.getString("orderQuantity").toString();
		}
	
		update_product_add_quantity(productID, quantity);
		
		sql_statement	=	"DELETE FROM orders WHERE orderID = '" + orderID + "' and productID = '" + productID + "';";		
		sql_query	=	con.prepareStatement(sql_statement);
		sql_query.executeUpdate();	
		
	}
	
	
	// Delete/Cancel an order
	public void delete_order(HttpServletRequest request) throws SQLException
	{	
		HttpSession session = request.getSession(true);	
		String	orderID		=	(String)session.getAttribute("orderID");
		
		String	sql_statement	=	"DELETE FROM orders WHERE orderID = '" + orderID + "';";
		sql_query	=	con.prepareStatement(sql_statement);
		sql_query.executeUpdate();
	}
		
	public int director_update_product( String productID,String productPrice, String productQuantity,  String productAvailable) throws SQLException
	{
		String	sql_statement;
		int query_result	=	0;
				
		if(!productPrice.isEmpty() && productPrice!=null)
		{
			sql_statement	=	"UPDATE product SET productPrice = '" + productPrice + "' WHERE productID = '" + productID + "';";			
			sql_query	=	con.prepareStatement(sql_statement);
			query_result	=	sql_query.executeUpdate();
		}
		if(productQuantity!=null && !productQuantity.isEmpty() )
		{
			int	quantity	=	Integer.valueOf(productQuantity);
			
			sql_statement	=	"UPDATE product SET productQuantity = '" + quantity + "' WHERE productID = '" + productID + "';";
			sql_query	=	con.prepareStatement(sql_statement);
			query_result	=	sql_query.executeUpdate();
		}
		if(productAvailable!=null)
		{
			sql_statement	=	"UPDATE product SET productAvailable = '" + productAvailable + "' WHERE productID = '" + productID + "';";	
			sql_query	=	con.prepareStatement(sql_statement);
			query_result	=	sql_query.executeUpdate();
		}		
		return query_result;
	}
	
	
	// Update the number of products sold by product ID after added to order
	public void update_products_sold(String productID, String quantity) throws SQLException
	{
		String	sql_statement	=	"UPDATE Product SET productsSold = productsSold + " + quantity + " WHERE productID = '" + productID + "';";
		sql_query	=	con.prepareStatement(sql_statement);	
		sql_query.executeUpdate();
	}
	
	
	//Update products sold when item removed from order
	public void update_products_sold_delete(String productID) throws SQLException
	{
		String sql_statement = "SELECT orderQuantity from orders WHERE productID = '" + productID + "';";
		sql_query	=	con.prepareStatement(sql_statement);
		rs	=	sql_query.executeQuery();
		
		System.out.println(sql_statement);
		
		String	orderQuantity	=	null;
		
		while(rs.next())
		{
			orderQuantity	=	rs.getString("orderQuantity");	
			System.out.println(orderQuantity);
		}
		int	quantity	=	Integer.valueOf(orderQuantity);
	
		sql_statement	=	"UPDATE Product SET productsSold = productsSold - " + quantity + " WHERE productID = '" + productID + "';";
		sql_query	=	con.prepareStatement(sql_statement);	
		sql_query.executeUpdate();
	}
	
	
	// Update the number of products available for purchase left in the inventory
	public void update_product_subtract_quantity(String productID, String quantity) throws SQLException
	{
		String	sql_statement	=	"UPDATE Product SET productQuantity = productQuantity - " + quantity + " WHERE productID = '" + productID + "';";
		sql_query	=	con.prepareStatement(sql_statement);
		sql_query.executeUpdate();
		
		sql_statement	=	"SELECT productQuantity FROM product WHERE productID = '" + productID + "';";
		sql_query	=	con.prepareStatement(sql_statement);
		rs = sql_query.executeQuery();
		
		String productQuantity	=	null;
		
		while(rs.next())
		{
			productQuantity	=	rs.getString("productQuantity");
		}
	
		int convtQuantity	=	Integer.valueOf(productQuantity);
		
		if(convtQuantity==0)
		{
			sql_statement	=	"UPDATE product SET productAvailable = 'N' WHERE productID = '" + productID + "';";
			sql_query	=	con.prepareStatement(sql_statement);
			sql_query.executeUpdate();
		}
	}
	
	
	//Add products to the inventory
	public void update_product_add_quantity(String productID, String quantity) throws SQLException
	{
		String	sql_statement	=	"UPDATE Product SET productQuantity = productQuantity + " + quantity + " WHERE productID = '" + productID + "';";
		sql_query	=	con.prepareStatement(sql_statement);		
		sql_query.executeUpdate();
		
		int convtQuantity	=	Integer.valueOf(quantity);
		
		if(convtQuantity!=0)
		{
			sql_statement	=	"UPDATE product SET productAvailable = 'Y' WHERE productID = '" + productID + "';";
			sql_query	=	con.prepareStatement(sql_statement);
			sql_query.executeUpdate();
		}
	}
	
	
	// Create a complaint against someone using their email
	public void create_complaint(HttpServletRequest request, String againstEmail, String complaintDate, String complaintDescr) throws SQLException
	{
		HttpSession session = request.getSession(true);	
		
		String	filerEmail	=	session.getAttribute("sessionEmail").toString();
		
		String	sql_statement	=	"INSERT INTO complaints VALUES (?,?,?,?);";
		sql_query	=	con.prepareStatement(sql_statement);
		
		sql_query.setString(1, complaintDate);
		sql_query.setString(2, filerEmail);
		sql_query.setString(3, againstEmail);
		sql_query.setString(4, complaintDescr);
		
		sql_query.executeUpdate();
	}
	
	
	// Add a rating for a client or salesperson
	public int rate_by_order_id(String orderID, Double rating) throws SQLException
	{	int query_status	=	0;
		String	sql_statement	=	"SELECT clientEmail from orders WHERE orderID ='" + orderID + "';";
		sql_query	=	con.prepareStatement(sql_statement);
		rs	=	sql_query.executeQuery();
		String	email	=	null;
		
		System.out.println(sql_statement);
		
		while(rs.next())
		{
			email	=	rs.getString("clientEmail");
		}
		
		System.out.println(email);
		
		String	convtRating	=	String.valueOf(rating);
		
		sql_statement	=	"INSERT INTO ratings VALUES(?,?,?);";
		sql_query	=	con.prepareStatement(sql_statement);
		sql_query.setString(1, email);
		sql_query.setString(2, convtRating);
		sql_query.setString(3, orderID);
		query_status	=	sql_query.executeUpdate();
		
		update_client_rating(email);
		
		return query_status;
	}
	
	public void update_client_rating(String email) throws SQLException
	{
		String sql_statement = "select sum(rating) sumRating, count(rating) numOfRating from ratings where email = '" + email + "';";
		sql_query	=	con.prepareStatement(sql_statement);
		
		System.out.println(sql_statement);
		
		rs = sql_query.executeQuery();
		String	sumRating	=	null;
		String	numOfRating	=	null;
		String	avgRating	=	null;
		double	convtSumRating		=	0;
		int		convtNumOfRating	=	0;
		double	averageRating		=	0;
		
		while(rs.next())
		{
			sumRating	=	rs.getString("sumRating");
			numOfRating	=	rs.getString("numOfRating");
		}
		convtSumRating		=	Double.valueOf(sumRating);
		convtNumOfRating	=	Integer.valueOf(numOfRating);
		
		averageRating	=	convtSumRating/convtNumOfRating;
		avgRating	=	String.valueOf(averageRating);
		
		sql_statement	=	"UPDATE user SET avgRating = " + avgRating + " WHERE email = '" + email + "';";
		
		System.out.println(sql_statement);
		
		sql_query	=	con.prepareStatement(sql_statement);
		sql_query.executeUpdate();
		
	}
}
