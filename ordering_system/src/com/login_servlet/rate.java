package com.login_servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class rate
 */
@WebServlet("/rate")
public class rate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public rate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String	url	=	"http://localhost:8080/ordering_system/";
		String	rate	=	request.getParameter("rate");
		int query_result	=	0;
		
		if(rate!=null)
		{
			String	orderID	=	request.getParameter("orderID");
			String	rating	=	request.getParameter("rating");
			
			Double convtRating	=	Double.valueOf(rating);
			
			try {
				Database	db	=	new Database();
				query_result	=	db.rate_by_order_id(orderID, convtRating);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(query_result!=0)
			{
				response.sendRedirect( url + "rating_added.jsp");
			}
			else if(query_result==0)
			{
				response.sendRedirect(url + "rating_add_failed.jsp");
			}
		}
	}

}
