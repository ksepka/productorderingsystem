package com.login_servlet;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;




/**
 * Servlet implementation class Register_servlet
 */
@WebServlet("/Register_servlet")
public class Register_servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String firstName, lastName, email, password1, password2;
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = 
		    Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE); 
	Statement st;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Register_servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("login.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String msg = " ";
		PrintWriter out = response.getWriter();
		
		//Declare and initialize variables
		String register = request.getParameter("register");
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String email = request.getParameter("email");
		String userType = request.getParameter("userType");
		String password = request.getParameter("password1");
		String confirm_password = request.getParameter("password2");
		Matcher m = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
		Database db = null;
		
		if(register!=null){
			//Check for empty fields
			if (firstName.isEmpty() || lastName.isEmpty() || email.isEmpty() || password.isEmpty() || confirm_password.isEmpty()){
				msg = "There are empty fields.  Please try again";
				response.setContentType("text/html");
				out.println("<font size='6' color=red>" + msg);
			}
			else{
				//Check whether the email address is valid
				if(!m.find()){
					msg = "email address is invalid.  Please try again";
					response.setContentType("text/html");
					out.println("<font size='6' color=red>" + msg);
				}
				else{
					//Check whether the passwords are entered correctly
					if(password.equals(confirm_password)){
						try{
							db = new Database();
							if(db.isEmailRegistered(email)){
								msg = "There is already an account for this email address.";
								response.setContentType("text/html");
								out.println("<font size='6' color=red>" + msg);
							}
							else{
								//At this point, everything is correct.  Thus, access to the DB is granted
								db.registerUser(firstName, lastName, email, userType, password);
								response.sendRedirect("registration_success.jsp");
							}
							
							db.disconnect();
						}
						catch (Exception e){
							out.println("Exception: " + e);
						}
					}
					else{
						msg = "password must match on both fields.  Please try again";
						response.setContentType("text/html");
						out.println("<font size='6' color=red>" + msg);
					}
				}
			}
		}
	}
	
	public String getFirstName(){
		return firstName;
	}
	
	public String getLastName(){
		return lastName;
	}
	
	public String getEmail(){
		return email;
	}
	
	public String getPassword1(){
		return password1;
	}
	
	public String getPassword2(){
		return password2;
	}
	
	public void setFirstName(String firstn){
		firstName = firstn;
	}	
	
	public void setLastName(String lastn){
		lastName = lastn;
	}	

	public void setEmail(String eml){
		email = eml;
	}

	public void setPassword1(String p1){
		password1 = p1;
	}

	public void setPassword2(String p2){
		password2 = p2;
	}
	

}
