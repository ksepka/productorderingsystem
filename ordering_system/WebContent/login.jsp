<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Login</title>
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,700">
<link rel="stylesheet" href="style.css">
</head>

  <body>
   	<h1 align='middle' > Manufacturer's Depot </h1>
    <div class="container">
      <div id="login">
        <form action="Login_servlet">
          <fieldset class="clearfix">
            <p><span class="fontawesome-email"></span><input type="text" name="email" placeholder="Email" onBlur="if(this.value == '') this.value = 'Email'" onFocus="if(this.value == 'Email') this.value = ''" required></p> <!-- JS because of IE support; better: placeholder="Username" -->
            <p><span class="fontawesome-lock"></span><input type="password" name="password" placeholder="Password" onBlur="if(this.value == '') this.value = 'Password'" onFocus="if(this.value == 'Password') this.value = ''" required></p> <!-- JS because of IE support; better: placeholder="Password" -->
            <p><input value="Sign In" type="submit"></p>
          </fieldset>
        </form>

        <p>Not a member? <a href="register.jsp">Sign up now</a><span class="fontawesome-arrow-right"></span></p>

      </div> <!-- end login -->
    </div>

  </body>
</html>