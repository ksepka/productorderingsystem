<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Register</title>
<link rel="stylesheet" href="style.css">
</head>
<body>

<div class="container">
      <div id="login">
        <form action="Register_servlet" method="post">
          <fieldset class="clearfix">
            	<p><span class="fontawesome-name"></span>
            		<input type="text" name="firstName" placeholder="First Name" onBlur="if(this.value == '') this.value = 'First Name'" onFocus="if(this.value == 'First Name') this.value = ''" required></p> 
            	<p><span class="fontawesome-name"></span>
            		<input type="text" name="lastName" placeholder="Last Name" onBlur="if(this.value == '') this.value = 'Last Name'" onFocus="if(this.value == 'Last Name') this.value = ''" required></p> 
            	<p><span class="fontawesome-user"></span>
            		<input type="text" name="email" placeholder="Email" onBlur="if(this.value == '') this.value = 'Email'" onFocus="if(this.value == 'Email') this.value = ''" required></p> 
            	
            	<p><span class="fontawesome-user"></span>
            		User Type: 
            		<select  name="userType">
						<option> Client </option>
						<option> Salesperson </option>
						<option> Manager </option>
						<option> Director </option>
					</select>
            	
            	<p><span class="fontawesome-lock"></span>
            		<input type="password" name="password1" placeholder="Password" onBlur="if(this.value == '') this.value = 'Password'" onFocus="if(this.value == 'Password') this.value = ''" required></p> 
            	<p><span class="fontawesome-lock"></span>
            		<input type="password" name="password2" placeholder="Confirm Password" onBlur="if(this.value == '') this.value = 'Confirm Password'" onFocus="if(this.value == 'Confirm Password') this.value = ''" required></p> <!-- JS because of IE support; better: placeholder="Confirm Password" -->
            	            	
            	<p><input name="register" type="submit"></p>
          </fieldset>
        </form>

      </div> <!-- end login -->
    </div>

</body>
</html>