<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Create Order</title>
</head>

<sql:setDataSource
        var="pos"
        driver="com.mysql.jdbc.Driver"
        url="jdbc:mysql://localhost:3306/product_ordering_system"
        user="root" password=""
    />

<body>

	<div style = ' border-style: solid; border-width: 2px;' align='middle'>
		<h1>Create/Edit Order
		<br>
		<form action='order_product' method='POST'>
		<input type='submit' name='deleteOrder' value='Delete Order'>
		<input type='submit' name='return' value='Return to Main Page'>
		</form>
		</h1>
	</div>
	
	<br>
	
	<div class='container'>
		<div style = ' border-style: solid; border-width: 1px; float:left'>
			<center>
				<h3>Edit Order</h3>
				<small>
					Pressing delete will 
					<br>
					Remove whole product order.
				
				</small>
			
			
			<div align='right'  style='width:100%'>
			<br>
			<form action='order_product' method='POST'>
				Product ID:	<input type='text' name='productID' >
				<br>
				Product Quantity: <input type='text' name='quantity'>
				<center>
					<input type='submit' name='create'value='Add '>
					<input type='submit' name='delete'value='Delete '>
				</center>	
				<br>		
			</form>
			</div>
		</div>
		
		
		
		<div class='container' >
		
			<sql:query var="order"   dataSource="${pos}">
        		SELECT orderID, productID, productName, orderQuantity, productPrice, totalPrice FROM orders where 
        		orderID = '<%= session.getAttribute("orderID") %>';
    		</sql:query>
			
			<center>
				
				<div align="center" style="height:170px;overflow:auto;width=100%">
        			<table border="1" cellpadding="5">
         
            			<tr>
            				<th>Order ID</th>
                			<th>Product ID</th>
                			<th>Product Name</th>
                			<th>Quantity</th>
                			<th>Product Price</th>
                			<th>Total Price</th>
                
           				</tr>

            			<c:forEach var="order" items="${order.rows}">
                		<tr>
                			<td><c:out value="${order.orderID}"></c:out>
                    		<td><c:out value="${order.productID}" /></td>
                    		<td><c:out value="${order.productName}" /></td>
                    		<td><c:out value="${order.orderQuantity }"/></td> 
                    		<td><c:out value="${order.productPrice}" /></td>
                    		<td><c:out value="${order.totalPrice }"/></td> 
              
                		</tr>
            			</c:forEach>
        			</table>
    			</div> 
			</center>
			
		
		
	</div>
	
	<br>
	<br>
	<br>
	
  <div class='container'>
	<sql:query var="products"   dataSource="${pos}">
        SELECT * FROM Product where productAvailable='Y';
    </sql:query>
    
    <caption><h2 align='center'>Available Products</h2></caption>
    
    <div align="center" style="height:275px;overflow:auto;">
        <table border="1" cellpadding="5">
         
            <tr>
                <th>Product Id</th>
                <th>Product Category</th>
                <th>Product Name</th>
                <th>Product Description</th>
                <th>Product Quantity</th>
                <th>Product Price</th>
           	</tr>

            <c:forEach var="products" items="${products.rows}">
                <tr>
                    <td><c:out value="${products.productID}" /></td>
                    <td><c:out value="${products.productCateg}" /></td>
                    <td><c:out value="${products.productName}" /></td>
                    <td><c:out value="${products.productDescr}" /></td>
                    <td><c:out value="${products.productQuantity}" /></td>
                    <td><c:out value="${products.productPrice}" /></td>                
                </tr>
            </c:forEach>
        </table>
    </div> 
</div>   

    

</body>

</html>