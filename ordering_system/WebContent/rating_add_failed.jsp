<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta  http-equiv="Refresh" content="2;url=salesperson.jsp">
<title>Rating Failed</title>
</head>
<body>
<p>Failed to add rating. Make sure you haven't rated the order's owner yet.
</body>
</html>