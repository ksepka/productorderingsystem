<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Salesperson</title>
</head>

<body>
	<div style = ' border-style: solid; border-width: 3px; width:100%' align='middle'>
		<h1>Welcome Salesperson, </h1>
		<h4><%=session.getAttribute("name") %>  
			<form action='log_out' method = 'GET'>
				<input type='submit' name='logout' value='Logout'>
			</form>
		</h4>
	</div>
	
	<br>
	
	<div class='container'>
		<div style = ' border-style: solid; border-width: 1px; float:left'>
			<center>
				<h3>Create/View  Order</h3>
				<small>
					To view order, 
					<br>
					provide order ID 
					<br>
					and enter n in other fields.	
				</small>
			</center>
			
			<br>
			
			<div align='right'  style='width:100%'>
			<form action='new_order' method='POST'>
				Order ID:	<input type='text' name='orderID' >
				<br>
				Client Email: <input type='text' name='clientEmail'>
				<br>
				Client Company:	<input type='text' name='clientCo'>
				<br>
				Order Date: <input type='text' name ='date' value="DD/MM/YYYY">
				<br>
				<center>
					<input type='submit' name='create' value='Create/View Order'>
				</center>	
				<br>		
			</form>
			</div>
		</div>
		
		<div class='container' style = ' float:right; width:70% '>
		
		<sql:setDataSource
        	var="pos"
        	driver="com.mysql.jdbc.Driver"
        	url="jdbc:mysql://localhost:3306/product_ordering_system"
        	user="root" password=""
    	/>
			
		<sql:query var="order"   dataSource="${pos}">
			select orderID, orderDate, clientEmail, companyName, sum(orderQuantity) quantity, 
			sum(totalPrice) price from orders where orderID = orderID and 
			salespersonEmail = '<%=session.getAttribute("sessionEmail").toString() %>' 
			group by orderID;
    	</sql:query>
    	
			<center>
				
				<div align="center" style="height:175px;overflow:auto; width:100%; ">
        			<table border="1" cellpadding="5">
         
            			<tr>
            				<th>Order ID</th>
                			<th>Order Date</th>
                			<th>Client Email</th>
                			<th>Company Name</th>
                			<th># of Products</th>
                			<th>Total Price</th>
           				</tr>

            			<c:forEach var="order" items="${order.rows}">
                		<tr>
                			<td><c:out value="${order.orderID}"></c:out>
                			<td><c:out value="${order.orderDate}"></c:out>
                    		<td><c:out value="${order.clientEmail}" /></td>
                    		<td><c:out value="${order.companyName}" /></td>
                    		<td><c:out value="${order.quantity}"/></td> 
                    		<td><c:out value="${order.price}" /></td> 
              
                		</tr>
            			</c:forEach>
        			</table>
    			</div>
    			 
			</center>
			<br>
			
		</div>
		<br>
	</div>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	
	
	<div class='container'>
		<div style = ' border-style: solid; border-width: 1px; float:left'>
			<center>
				<h3>File a Complaint</h3>
			</center>
			
			
			<div align='right'  style='width:100%'>
			<form action='file_complaint' method='POST'>
				Complaint Date:	<input type='text' name='complaintDate' value='DD/MM/YYYY' >
				<br>
				Complaint Against: <input type='text' name='complaintAgainst'>
				<br>
				<label> 
				Complaint Description:	<textarea row='5' name='complaintDescr'></textarea>
				</label>
				<br>
				
				<center>
					<input type='submit' name='complaint' value='File Complaint'>
				</center>	
				<br>		
			</form>
			</div>
		</div>
		
			
			<div style = ' border-style: solid; border-width: 1px; float:right; width:65%'>
				<center><strong>Rate By Order ID</strong></center>
				<br>
				<div style="float:right;">
				<form action='rate' method='POST'>
				Order ID: <input type='text' name='orderID'>
				Rating: <input type='text' name='rating'>
				<input type='submit' name='rate' value="Rate">
				</form>
				
				</div>
			</div>
			
	</div>
	
	
	<div class='container' style = ' float:right; width:100% '>
		<br>
		<br>
		 <sql:update var="drop" dataSource="${pos }">
			drop table if exists tempStats;
		</sql:update> 
		
		
		<sql:update var="tempStats"   dataSource="${pos}">		
        	create table tempStats
        	(select sum(orderQuantity) productsSold, 
        	sum(totalPrice) totalProductAmounts,
        	salespersonEmail  
        	from orders where orderID = orderID and 
        	salespersonEmail = '<%=session.getAttribute("sessionEmail").toString() %>'
        	group by orderID );
        </sql:update>
        
        <sql:query var="stats"   dataSource="${pos}">
        	select count(productsSold) numOforders,
        	sum(productsSold) totalProductsSold, 
        	sum(totalProductAmounts) netSum, 
        	sum(totalProductAmounts)*.05 totalCommission
        	from tempStats
        	where salespersonEmail = '<%=session.getAttribute("sessionEmail").toString() %>';
        	
        	
    	</sql:query>
    	
    	<sql:query var = "likes" dataSource="${pos}">
    		select totalLikes, totalDislikes, numberOfComplaints 
    		from user
    		where email = '<%=session.getAttribute("sessionEmail").toString() %>';
    	</sql:query>
    
		
			<center>
				
				<div  style="height:175px;overflow:auto; width:100%; float:left">
        			<strong>Statistics</strong>
        			<br>
        			<br>
        		
        				<table border="1" cellpadding="5">
         
            			<tr>
            				<th># of Orders</th>
                			<th># of Products Sold</th>
                			<th>Net Sum</th>
                			<th>Total Commission</th>
                			<th># of Likes</th>
                			<th># of Dislikes</th>
                			<th># of Complaints</th>
                
           				</tr>
           				
           				<c:forEach var="stats" items="${stats.rows}">
                		<tr>
                			<td><c:out value="${stats.numOfOrders}"></c:out>
                			<td><c:out value="${stats.totalProductsSold}"></c:out>
                    		<td><c:out value="${stats.netSum}" /></td>
                    		<td><c:out value="${stats.totalCommission}" /></td>
                    	</c:forEach>
                    	
                    	<c:forEach var="likes" items="${likes.rows}">
                    		<td><c:out value="${likes.totalLikes}" /></td>
                    		<td><c:out value="${likes.totalDislikes}"/></td> 
                    		<td><c:out value="${likes.numberOfComplaints}" /></td> 
                    	</c:forEach>
                    	
               			</tr>
               			
               				
               				
           				
           				
        			</table>
        			
    			</div>
    			
    			
    			 
			</center>
			
		</div>
		
	
</body>
</html>