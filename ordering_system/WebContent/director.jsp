<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Director</title>
</head>

<body>

<sql:setDataSource
        var="pos"
        driver="com.mysql.jdbc.Driver"
        url="jdbc:mysql://localhost:3306/product_ordering_system"
        user="root" password=""
    />
    

	<div style = ' border-style: solid; border-width: 3px;' align='middle'>
		<h1>Welcome Director,</h1>
			<h4><%=session.getAttribute("name") %>  
			<form action='log_out' method = 'GET'>
				<input type='submit' name='logout' value='Logout'>
			</form>
		</h4>	
	</div>
	
	<sql:query var="products"   dataSource="${pos}">
        SELECT * FROM Product;
    </sql:query>
    
    <caption><h2 align='center'>Products</h2></caption>
    
    <div align="center" style="height:275px;overflow:auto;">
        <table border="1" cellpadding="5">
         
            <tr>
                <th>Product Id</th>
                <th>Product Category</th>
                <th>Product Name</th>
                <th>Product Description</th>
                <th>Product Quantity</th>
                <th>Product Price</th>
                <th>Product Available</th>
                <th>Products Sold</th>
           	</tr>

            <c:forEach var="products" items="${products.rows}">
                <tr>
                    <td><c:out value="${products.productID}" /></td>
                    <td><c:out value="${products.productCateg}" /></td>
                    <td><c:out value="${products.productName}" /></td>
                    <td><c:out value="${products.productDescr}" /></td>
                    <td><c:out value="${products.productQuantity}" /></td>
                    <td><c:out value="${products.productPrice}" /></td>
                    <td><c:out value="${products.productAvailable}" /></td>
                    <td><c:out value="${products.productsSold}" /></td>
                
                </tr>
            </c:forEach>
        </table>
    </div>
	
	
	<br>

	<div class='container'>
		<div style = ' border-style: solid; border-width: 1px; float:left'>
			<center>
				<h3>Insert/Update Product</h3>
				<small>Update only available for 
					<br>
					Price, Quantity, & Availability
					<br>
					Using the Product ID
				</small>
			</center>
			
			<br>
			
			<div align='right'  style='width:100%'>
			<form action='insert_product' method='POST'>
				Product ID:	<input type='text' name='productID' >
				<br>
				Product Category: <input type='text' name='productCateg'>
				<br>
				Product Name:	<input type='text' name='productName'>
				
				<br>
				<label> 
				Product Descriptions:	<textarea row='5' name='productDescr'></textarea>
				<label>
				<br>
				Product Quantity:	<input type='text' name='productQuantity'>
				<br>
				Product Price:	<input type='text' name='productPrice'>
				<br>
				<label>
				Product Available:	Yes<input type='radio' value='Y' name='productAvailable' >
									No<input type='radio'  value='N' name='productAvailable' >
				</label>
				<br>
				<center>
					<input type='submit' name='create'value='Insert'>
					<input type='submit' name='update' value='Update'>	
				</center>	
				<br>		
			</form>
			</div>
		</div>
		
		<div class='container' style = ' border-style: solid; border-width: 1px; float:right; width:60%''>
			<h3 align='center'>Best/Worst Performing Salesgroups</h3>
		</div>
		<br>
	</div>
	
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<sql:query var="order"   dataSource="${pos}">
        select orderID, orderDate, clientEmail, salespersonEmail, companyName, sum(orderQuantity) quantity,sum(totalPrice) price 
        from orders 
        where orderID = orderID 
        group by orderID;
    </sql:query>
    
    <caption><h2 align='center'>Orders</h2></caption>
    
    <div align="center" style="height:275px;overflow:auto;">
        <table border="1" cellpadding="5">
         
            <tr>
                <th>Order Id</th>
                <th>Order Date</th>
                <th>Company Name</th>
                <th>Client Email</th>
                <th>Salesperson Email</th>
                <th>Order Quantity</th>
                <th>Total Price</th>
           	</tr>

            <c:forEach var="order" items="${order.rows}">
                <tr>
                    <td><c:out value="${order.orderID}" /></td>
                    <td><c:out value="${order.orderDate}" /></td>
                    <td><c:out value="${order.companyName}" /></td>
                    <td><c:out value="${order.clientEmail}" /></td>
                    <td><c:out value="${order.salespersonEmail}" /></td>
                    <td><c:out value="${order.quantity}" /></td>
                    <td><c:out value="${order.price}" /></td>
                
                </tr>
            </c:forEach>
        </table>
    </div>

</body>
</html>