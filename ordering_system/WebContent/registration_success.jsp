<!DOCTYPE html>
<html>
<%@taglib prefix="t" uri="http://java.sun.com/jsp/jstl/core" %>

<head>
  <meta charset="UTF-8">
  <title>Manufacturer's Depot</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>

  <html lang="en-US">
  <head>
    <meta charset="utf-8">
    <title>Login</title>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,700">

    <!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->

  </head>

  <body>
	<p>New account registered.  Please sign in: <span class="fontawesome-arrow-right"></span></p>
    <div class="container">
      <div id="login">
        <form action="Login_servlet">
          <fieldset class="clearfix">
            <p><span class="fontawesome-email"></span><input type="text" name="email" value="Email" onBlur="if(this.value == '') this.value = 'Email'" onFocus="if(this.value == 'Email') this.value = ''" required></p> <!-- JS because of IE support; better: placeholder="Username" -->
            <p><span class="fontawesome-lock"></span><input type="password" name="password" value="Password" onBlur="if(this.value == '') this.value = 'Password'" onFocus="if(this.value == 'Password') this.value = ''" required></p> <!-- JS because of IE support; better: placeholder="Password" -->
            <p><input value="Sign In" type="submit"></p>
          </fieldset>
        </form>



      </div> <!-- end login -->
    </div>

  </body>
</html>