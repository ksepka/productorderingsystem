<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Refresh" content="3; url=new_order.jsp">
<title>Invalid Quantity</title>
</head>
<body>

<p>
	The requested quantity of the product is unavailable.
</p>
<p>
	Please verify the quantity is available and try again.
</p>
<p>
	Redirecting ....
</p>

</body>
</html>